package keyStore;

public class KeyStore01 {

	private String[] keyStore01;
	private int MAX_VALUE =100;
	
	public KeyStore01() {
		keyStore01 = new String[100];
	}
	public KeyStore01(int length) {
		
		keyStore01 = new String[length];
		
	}
	
	public java.lang.String get(int index) {
		String search = null;
		for(int i=0;i<MAX_VALUE;i++) {
			if(keyStore01[i]==search) {
				return keyStore01[i];
			}
		}
		
		return null;
	}
	
	public int size() {
		int value = 0;
		for(int i=0;i<MAX_VALUE;i++) {
			if(keyStore01[i]!=null) {
				value++;
			}
			if(value>MAX_VALUE) {
				return MAX_VALUE;
			}
		}
		return value;
	}
	
	
	
}

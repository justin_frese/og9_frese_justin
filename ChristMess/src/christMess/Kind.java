package christMess;

import java.util.*;
import javax.print.attribute.standard.MediaSize.Other;

public class Kind implements Comparable{
	
	  //Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	  //Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode
	
	private String vorname;
	private String nachname;
	private String geburtsdatum;
	private String ort;
	private int bravheitheitsgrad;
	
	public Kind(String vorname, String nachname,String geburtstag,String geburtsdatum,String ort,int bravheitheitsgrad) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.ort = ort;
		this.bravheitheitsgrad = bravheitheitsgrad;
	}
	
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getGeburtssdatum() {
		return geburtsdatum;
	}
	public void setGeburtsdatum(String geburtstag) {
		this.geburtsdatum = geburtstag;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String wohnort) {
		this.ort = ort;
	}
	public int getBravheitsgrad() {
		return bravheitheitsgrad;
	}
	public void setBravheitsgrad(int bravheit) {
		this.bravheitheitsgrad = bravheit;
	}
	
	@Override
final public String toString() {
	return "Kind [Vorname="+vorname+", Nachname="+nachname+", Geburtsdatum="+geburtsdatum+" ,Ort="+ort+", Bravheitheitsgrad"+bravheitheitsgrad+"]";

}
	@Override
public int compareTo(Object o) {
	return this.nachname.compareTo(nachname);
}
	
	
	
}
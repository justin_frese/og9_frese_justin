package primzahl;
import java.util.Scanner;
public class Primzahl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Gebe eine Zahl ein");
		long zahl = scan.nextLong();
		System.out.println(isPrim(zahl));
		
		
	}
	
	
	public static boolean isPrim(long zahl) {
		zahl = Math.abs(zahl);
		if (zahl < 2) {
			return false;
		
		}
		double sqrt = Math.sqrt(zahl);
		for(long i = 2; i <= sqrt; i++) {
			if(zahl % i ==0) {
				return false;
			}
		}
		return true;
	}

}

package primzahl;

import java.util.Scanner;

public class TestPrim {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner scan = new Scanner(System.in);
		System.out.println("Gebe eine Zahl ein");
		long zahl = scan.nextLong();
		
		
		long start = System.currentTimeMillis();
		if(Primzahl.isPrim(zahl)) {
			System.out.println("Primzahl");
		}else {
			System.out.println("Keine Primzahl");
		}
		long ende = System.currentTimeMillis();
		System.out.println("Zeit: "+((ende - start)/1000.0));
	}

}

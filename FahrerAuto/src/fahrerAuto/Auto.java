package fahrerAuto;

public class Auto {

	private String kennzeichen;
	private Fahrer fahrer;
	
	
	
	public String getKennzeichen() {
		return kennzeichen;
	}
	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}
	
	public Fahrer getFahrer() {
		return fahrer;
	}
	public void setFahrer(Fahrer fahrer) {
		this.fahrer = null;
		this.fahrer = fahrer;
		fahrer.setAuto(this);
	}
	public void unlinkFahrer() {
		fahrer = null;
	}
	
}

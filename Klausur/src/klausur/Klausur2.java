package klausur;

import java.util.Random;

public class Klausur2 {

  public final int NICHT_GEFUNDEN = -1;
  
  public static void main(String[] args) {
    Klausur2 k2 = new Klausur2();
    int n = 1000; //Anzahl der Stellen
    int n2 = 10000;
    
    //Erstellt und sortiert Array mit n Stellen
    long[] zahlen = k2.getSortedIntArray(n);
    long[] zahlen2 = k2.getSortedIntArray(n2);
    int gesuchteZahl = 200000;
   
    
    //----------Hier in die Main k�nnen Sie Ihre Analyse f�r das Protokoll schreiben--------------
    k2.lineareSuche(zahlen, gesuchteZahl);
    System.out.println("LineareSuche mit 1000 Stellen: "+countLinear);
    k2.lineareSuche(zahlen2, gesuchteZahl);
    System.out.println("LineareSuche mit 10000 Stellen: "+countLinear);
    
    k2.schlaubiSchlumpfSuche(zahlen, gesuchteZahl);
    System.out.println("SchlaubiSchlumpfSuche mit 1000 Stellen: "+countSchlaubiSchlumpf);
    k2.schlaubiSchlumpfSuche(zahlen2, gesuchteZahl);
    System.out.println("SchlaubiSchlumpfSuche mit 10000 Stellen: "+countSchlaubiSchlumpf);
    
    k2.binaereSuche(zahlen, gesuchteZahl);
    System.out.println("Bin�reSuche mit 1000 Stellen "+countBinaereSuche);
    k2.binaereSuche(zahlen2, gesuchteZahl);
    System.out.println("Bin�reSuche mit 10000 Stellen: "+countBinaereSuche);
    
  
  }
  static int countLinear;
  static int countSchlaubiSchlumpf;
  static int countBinaereSuche;

  public int lineareSuche(long[] zahlen, long gesuchteZahl) {
    for (int i = 0; i < zahlen.length; i++) {
    	countLinear++;
      if (zahlen[i] == gesuchteZahl) 
        return i;
      }
      
    return NICHT_GEFUNDEN;
  }
  
  public int schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
    int l = zahlen.length/4 - 1;
    while(l < zahlen.length && l >= 0){
      if(gesuchteZahl == zahlen[l]) {
    	  countSchlaubiSchlumpf++;
        return l;
        }
      if(gesuchteZahl < zahlen [l]) {
        l--;
      countSchlaubiSchlumpf++;
    }else {
        l = l + zahlen.length/4;
      countSchlaubiSchlumpf++;
      }
      }
    return NICHT_GEFUNDEN;
  }

  public int binaereSuche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;

    while (l <= r) {
      // Bereich halbieren
      m = l + ((r - l) / 2);

      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return m;
      countBinaereSuche++;

      if (zahlen[m] > gesuchteZahl) {
        r = m - 1; // im linken Abschnitt weitersuchen
      countBinaereSuche++;
      }else {
        l = m + 1; // im rechten Abschnitt weitersuchen
      countBinaereSuche++;
    }}
    return NICHT_GEFUNDEN;
  }
  
//-----------------------Finger weg von diesem Teil des Codes 
  
  /**
   * Methode f�llt das Attribut zahlen mit einem Array der L�nge des
   * Paramaters z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
   * 
   * @param stellen
   *            - Anzahl der zuf�lligen Stellen
   * @return das Array
   */
  public long[] getSortedIntArray(int stellen) {
    Random rand = new Random(666L);
    long[] zahlen = new long[stellen];
    for (int i = 0; i < stellen; i++)
      zahlen[i] = rand.nextInt(20000000);
    this.radixSort(zahlen);
    return zahlen;
  }

  /**
   * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
   * //NoHintAvailable
   */
  public void radixSort(long[] zahlen) {
    long[] temp = new long[20000000];
    for (long z : zahlen)
      temp[(int)z]++;
    int stelle = 0;
    for (int i = 0; i < temp.length; i++)
      if (temp[i] != 0)
        while (temp[i]-- != 0)
          zahlen[stelle++] = i;
  }

}
package interpolationssuche;

import randomList.RandomList;

public class Interpolationssuche {

	public static int Interpolationssuche(long gesuchteZahl) {
		
		int links = 0;
		int rechts = RandomList.randomList().length -1;
		long versch;
		int pos;
		
		while(gesuchteZahl >= RandomList.randomList()[links] && gesuchteZahl <= RandomList.randomList()[rechts] ) {
			versch = RandomList.randomList()[rechts] - RandomList.randomList()[links];
			
			pos = links + (int)(((double)rechts-links)*(gesuchteZahl - RandomList.randomList()[links])/versch);
			
			if( gesuchteZahl > RandomList.randomList()[pos]) {
				links = pos+1;
			}else if(gesuchteZahl < RandomList.randomList()[pos]) {
				rechts = pos-1;
			}else
				return pos;
		}
		
		
		
		return -1;
	}
	
	
	
	
	
	
	
	
}

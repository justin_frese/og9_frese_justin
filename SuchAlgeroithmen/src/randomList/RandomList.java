package randomList;
import java.util.Arrays;
import java.util.Random;
public class RandomList {

	public static long[] randomList() {
		
		long[] liste = new long[20];
		Random randNum = new Random();
		for (long i = 0 ; i < 20 ; i++) {
			liste[(int) i] = randNum.nextInt(1000_000_000);
		}
		Arrays.sort(liste);
		return liste;
		
		
	}
}
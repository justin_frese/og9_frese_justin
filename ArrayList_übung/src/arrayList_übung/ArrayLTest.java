package arrayList_�bung;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;
/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 13.08.2019
 * @author  
  * �bung zur ArrayList: Gibt Zahlen r�ckw�rts ein und aus.
 */

public class ArrayLTest {
  
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    ArrayList <Integer> al  = new ArrayList<Integer>() ;
    System.out.println("Gib die L�nge der Liste ein, ACHTUNG TRICK");
    int laenge = sc.nextInt();

    for (int i = 1  ;i < laenge+1 ; i++ ) {   // +1 ist Absicht: Nennen Sie den Unterschied zu einem Array
      al.add(i);
    } // end of for

    ListIterator <Integer> li = al.listIterator();
    while (li.hasNext()) { 
      System.out.println(li.next());
    } // end of while

    System.out.println();

    al.remove (Integer.valueOf(2));  // den Wert 2 falls vorhanden l�schen
    for (int ausgabe : al) {
      System.out.println(ausgabe);
    } // end of for

  } // end of main

} // end of class ArrayLTest

package eisenbahn;

public class Waggon {
	private int wagennr;
	private Zug zug;

	@Override
	public String toString() {
		return "Waggon [wagennr=" + wagennr + "]";
	}

	public Waggon() {
		this.wagennr = (int)(Math.random()*10000);
	}

	public Waggon(int wagennr) {
		super();
		this.wagennr = wagennr;
	}

	public int getWagennr() {
		return wagennr;
	}

	public void setWagennr(int wagennr) {
		this.wagennr = wagennr;
	}

	public Zug getZug() {
		return zug;
	}

	public void setZug(Zug zug) {
		this.zug = zug;
	}
}

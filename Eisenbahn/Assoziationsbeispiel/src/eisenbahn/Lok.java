package eisenbahn;

public class Lok {
	private int vMax;
	private Zug zug;

	public Lok(int vMax) {
		super();
		this.vMax = vMax;
	}

	public Zug getZug() {
		return zug;
	}

	public void setZug(Zug zug) {
		this.zug = zug;
	}

	public int getvMax() {
		return vMax;
	}

	public void setvMax(int vMax) {
		this.vMax = vMax;
	}

	@Override
	public String toString() {
		return "Lok [vMax=" + vMax + "]";
	}
	
	
}

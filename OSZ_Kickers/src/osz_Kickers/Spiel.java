package osz_Kickers;

public class Spiel {

	
	
	public boolean heimspiel;
	public String spielDatum;
	public int ergebnisHeimTore;
	public int ergebnisGastTore;
	public String[] hatGelbKarte;
	public String[] hatRoteKarte;
	
	
	
	
	public boolean isHeimspiel() {
		return heimspiel;
	}
	public void setHeimspiel(boolean heimspiel) {
		this.heimspiel = heimspiel;
	}
	public String getSpielDatum() {
		return spielDatum;
	}
	public void setSpielDatum(String spielDatum) {
		this.spielDatum = spielDatum;
	}
	public int getErgebnisHeimTore() {
		return ergebnisHeimTore;
	}
	public void setErgebnisHeimTore(int ergebnisHeimTore) {
		this.ergebnisHeimTore = ergebnisHeimTore;
	}
	public int getErgebnisGastTore() {
		return ergebnisGastTore;
	}
	public void setErgebnisGastTore(int ergebnisGastTore) {
		this.ergebnisGastTore = ergebnisGastTore;
	}
	public String[] getHatGelbKarte() {
		return hatGelbKarte;
	}
	public void setHatGelbKarte(String[] hatGelbKarte) {
		this.hatGelbKarte = hatGelbKarte;
	}
	public String[] getHatRoteKarte() {
		return hatRoteKarte;
	}
	public void setHatRoteKarte(String[] hatRoteKarte) {
		this.hatRoteKarte = hatRoteKarte;
	}
	
	
	
}

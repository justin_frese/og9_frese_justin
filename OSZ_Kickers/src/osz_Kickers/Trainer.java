package osz_Kickers;

public class Trainer extends Mitglied {

	
	private char lizenzklasse;
	private int Aufwandentschaedigung;
	
	public Trainer() {
		super();
	}
	
	public Trainer(char lizenzklasse, int Aufwandentschaedigung ,String name, long telefonnummer, boolean jahresbeitrag) {
		super(name, telefonnummer, jahresbeitrag);
		this.lizenzklasse = lizenzklasse;
		this.Aufwandentschaedigung = Aufwandentschaedigung;
	}
	
	
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public int getAufwandentschaedigung() {
		return Aufwandentschaedigung;
	}
	public void setAufwandentschaedigung(int aufwandentschaedigung) {
		Aufwandentschaedigung = aufwandentschaedigung;
	}
	
	
}

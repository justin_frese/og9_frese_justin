package osz_Kickers;

public class Schiedsrichter extends Mitglied {
	
	private int gepfiffeneSpiele;

	
	public Schiedsrichter() {
		super();
	}
	
	public Schiedsrichter(int gepfiffeneSpiele, String name, long telefonnummer, boolean jahresbeitrag) {
		super(name,  telefonnummer,  jahresbeitrag);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
	
	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
	

}

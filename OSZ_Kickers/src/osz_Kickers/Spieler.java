package osz_Kickers;

public class Spieler extends Mitglied {

	
	private int trikotnummer;
	private String spielerposition;
	
	public Spieler() {
		super();
	}
	
	public Spieler(int trikotnummer, String spielerposition, String name, long telefonnummer, boolean jahresbeitrag) {
		super(name, telefonnummer, jahresbeitrag);
		this.trikotnummer = trikotnummer;
		this.spielerposition = spielerposition;
		
		
	}
	
	
	
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getSpielerposition() {
		return spielerposition;
	}
	public void setSpielerposition(String spielerposition) {
		this.spielerposition = spielerposition;
	}
	
	
	
	
	
	
	
	
	
}

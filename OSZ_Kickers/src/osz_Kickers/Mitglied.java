package osz_Kickers;

public abstract class Mitglied {

	private String name;
	private long telefonnummer;
	private boolean jahresbeitrag;
	
	public Mitglied() {
		super();
	}
	
	public Mitglied(String name, long telefonnummer, boolean jahresbeitrag) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitrag = jahresbeitrag;
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitrag() {
		return jahresbeitrag;
	}
	public void setJahresbeitrag(boolean jahresbeitrag) {
		this.jahresbeitrag = jahresbeitrag;
	}
	
	
	
	
	
	

}

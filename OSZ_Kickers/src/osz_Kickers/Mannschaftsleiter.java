package osz_Kickers;

public class Mannschaftsleiter extends Spieler {

	
	private String mannschaftsName;
	private double jahresrabatt;
	
	public Mannschaftsleiter() {
		super();
	}
	
	public Mannschaftsleiter(String mannschaftsName, double jahresrabatt, int trikotnummer, String spielerposition, String name, long telefonnummer, boolean jahresbeitrag) {
		super(trikotnummer, spielerposition, name, telefonnummer, jahresbeitrag);
		this.mannschaftsName = mannschaftsName;
		this.jahresrabatt = jahresrabatt;
	}
	

	public String getMannschaftsName() {
		return mannschaftsName;
	}
	public void setMannschaftsName(String mannschaftsName) {
		this.mannschaftsName = mannschaftsName;
	}
	public double getJahresrabatt() {
		return jahresrabatt;
	}
	public void setJahresrabatt(double jahresrabatt) {
		this.jahresrabatt = jahresrabatt;
	}
	
	
	
	
}
